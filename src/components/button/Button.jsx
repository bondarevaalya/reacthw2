import React from 'react';
import { StyledButton } from './styled.js'

export class Button extends React.Component{
    constructor(props){
     super(props)
     this.props = props
    }
    render(){
        return(
            <StyledButton btn={this.props.btn} onClick={this.props.onClick}>{this.props.text}</StyledButton>
        )
    }
}