import styled from 'styled-components'; 

export const StyledButton = styled.button`
    padding: 10px;
    margin: 10px;
    border: none;
    border-radius: 3px;
background-color: ${props => props.btn};
`