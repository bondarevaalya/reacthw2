import React from 'react';
import { StyledShopItem, StyledImg } from './styled';
import { ReactComponent as FavSvgTrue } from './add-fav-true.svg';
import { ReactComponent as FavSvgFalse } from './add-fav-false.svg';
import PropTypes from 'prop-types';
import {Button} from '../button'


class ShopItem extends React.Component {

  render() {
    const {name, price, image, sku, color, id, isFav, backgroundColor, toggleFav, addCart, showModal} = this.props;
    return (
    <StyledShopItem>
      <StyledImg src={image} alt={name}/>
      <div onClick={() => {
        toggleFav(id);
      }}>
        {isFav ? <FavSvgTrue /> : <FavSvgFalse />}
      </div>
      <div>
        <h3>{name}</h3>
        <ul>
          <li>Color: {color}</li>
          <li>SKU: {sku}</li>
        </ul>
        <span>{price} ₴</span>
      </div> 
      <Button btn="red" onClick={() => 
        showModal(() => addCart(id), 'Add to cart?', 'Are you sure want to add this to cart?')
      }>Add to cart</Button>
    </StyledShopItem>
    );
  }
}

ShopItem.propTypes = {
  name: PropTypes.string.isRequired,
  price: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  image: PropTypes.string.isRequired,
  sku: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,

  toggleFav: PropTypes.func.isRequired,
  addCart: PropTypes.func.isRequired,
  showModal: PropTypes.func.isRequired,

  backgroundColor: PropTypes.string,
  isFav: PropTypes.bool,
}

ShopItem.defaultProps = {
  backgroundColor: 'lightgrey',
  isFav: false
}

export default ShopItem;