import styled from 'styled-components'; 

export const StyledShopItem = styled.section`
border-radius: 15px;
padding: 5px;
position: relative;
width: 250px;
display: flex;
flex-direction: column;
align-items: center;
`

export const StyledImg = styled.img`
  width: 240px;
  height: 200px;
  object-fit: cover;
  border: 1px solid gray;
  border-radius: 15px;
`
export const StyledSvg = styled.svg`
  position: absolute;
  right: 15px;
  top: 15px;
  cursor: pointer;
`
export const StyledPath = styled.path`
  fill: firebrick;
`