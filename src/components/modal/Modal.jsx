import { PureComponent } from "react";
import PropTypes from 'prop-types';
import {Button} from '../button'
import {StyledModal, StyledModalContent, StyledClose} from './styled'

class Modal extends PureComponent {
  render () {
    const { actionFn, title, text, hasCloseButton, hideFn } = this.props;
    const optionalCeneterd = {
      'justifyContent': hasCloseButton ? 'space-between' : 'center',
    }
    return (
      <StyledModal onClick={() => hideFn()}>
        <StyledModalContent onClick={e => e.stopPropagation()}>
            <h2>{title}</h2>
            {hasCloseButton && <StyledClose onClick={() => hideFn()}>&#10761;</StyledClose>}
            <p>{text}</p>
            <div>
              <Button btn="pink" onClick={() => {
                actionFn();
                hideFn();
              }}>Yes</Button>
              <Button btn="grey" onClick={() => hideFn()}>No</Button>
            </div>
          </StyledModalContent>
      </StyledModal>
    )
  }
}

Modal.propTypes = {
  actionFn: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  hideFn: PropTypes.func.isRequired,
  text: PropTypes.string.isRequired,
  hasCloseButton: PropTypes.bool,
}

Modal.defaultProps = {
  hasCloseButton: true
}

export default Modal;