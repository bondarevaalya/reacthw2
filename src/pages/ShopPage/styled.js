import styled from 'styled-components';

export const StyledShopPage = styled.section`
display: flex;
flex-wrap: wrap;
justify-content: center;
align-items: center;
margin: 20px;
gap: 10px;
`