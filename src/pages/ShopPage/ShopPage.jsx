import React from 'react'
import { StyledShopPage } from './styled';
import ShopItem from '../../components/shopItem';
import PropTypes from 'prop-types';

class ShopPage extends React.Component {
  render() {
    const { goods, toggleFav, addCart, showModal } = this.props;
    return (
      <StyledShopPage>
        {goods.length > 0 && goods.map(e => 
          <ShopItem {...e} 
            key={e.id} 
            toggleFav={toggleFav} 
            addCart={addCart}
            showModal={showModal}
          />
        )}
      </StyledShopPage>
    );
  }
}

ShopPage.propTypes = {
  goods: PropTypes.array.isRequired, 
  toggleFav: PropTypes.func.isRequired, 
  addCart: PropTypes.func.isRequired, 
  showModal: PropTypes.func.isRequired,
}

export default ShopPage;